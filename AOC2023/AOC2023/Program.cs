﻿namespace AOC2023
{
    internal static class Program
    {
        static void Main(string[] args)
        {
            // new Day01("Day01").SolveAndPrintResults();
            // new Day02("Day02").SolveAndPrintResults();
            // new Day03("Day03").SolveAndPrintResults();
            // new Day04("Day04").SolveAndPrintResults();
            // new Day05("Day05").SolveAndPrintResults();
            // new Day06("Day06").SolveAndPrintResults();
            // new Day07("Day07").SolveAndPrintResults();
            // new Day08("Day08").SolveAndPrintResults();
            new Day09("Day09").SolveAndPrintResults();
        }
    }
}
