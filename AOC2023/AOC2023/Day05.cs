﻿using AOCShared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOC2023
{
    internal class Day05 : Aoc<List<string>, long, long>
    {
        private List<long> _seeds;
        private Maps _maps;

        public Day05(string fileName) : base(fileName)
        {
            _seeds = GetSeeds(Input);
            _maps = new Maps(Input);
        }

        private List<long> GetSeeds(List<string> input)
        {
            var seeds = Input[0].Split(':')[1].Split(" ").Skip(1).Select(long.Parse).ToList();

            return seeds;
        }

        public override long GetResultPart1()
        {
            long theNearestLocation = long.MaxValue;
            foreach (var seed in _seeds)
            {
                var seedSchema = new SeedSchema(seed, _maps);
                if (seedSchema.LocationNumber < theNearestLocation) { 
                    theNearestLocation  = seedSchema.LocationNumber; 
                }
            }

            return theNearestLocation;
        }
        public override long GetResultPart2()
        {
            // Dictionary<long, long> seedRanges = GetSeedPairs().OrderBy(x => x.Key).ToDictionary(x => x.Key, x => x.Value);

            // int index = 0;
            // foreach (var range in seedRanges)
            // {
            // }
            //foreach (var key in seedPairs.Keys)
            //{
            //    long theNearestLocation = long.MaxValue;

            //    List<long> tempSeed = new List<long>();
            //    for (long i = key; i < key + 100000; i++)
            //    {
            //        tempSeed.Add(i);
            //    }

            //    foreach (var seed in tempSeed)
            //    {
            //        var seedSchema = new SeedSchema(seed, _maps);
            //        if (seedSchema.LocationNumber < theNearestLocation)
            //        {
            //            theNearestLocation = seedSchema.LocationNumber;
            //        }
            //    }
            //    theLowest.Add(theNearestLocation);
            //}

            return 0;

        }

        private Dictionary<long, long> GetSeedPairs()
        {
            Dictionary<long, long> result = new Dictionary<long, long>();
            int index = 1;
            foreach (var seed in _seeds)
            {
                if (index % 2 != 0)
                {
                    result.Add(_seeds[index - 1], _seeds[index - 1] + _seeds[index]);
                }
                index++;
            }
            
            return result;
        }
    }

    public class Maps 
    {
        public Maps(List<string> input) {
            SeedsToSoil = GetTheMaps(input, "seed-to-soil map:");
            SoilToFertilizer = GetTheMaps(input, "soil-to-fertilizer map:");
            FertilizerToWater = GetTheMaps(input, "fertilizer-to-water map:");
            WaterToLight = GetTheMaps(input, "water-to-light map:");
            LightToTemp = GetTheMaps(input, "light-to-temperature map:");
            TempToHumidity = GetTheMaps(input, "temperature-to-humidity map:");
            HumidityToLocation = GetTheMaps(input, "humidity-to-location map:");
        }

        public List<Map> SeedsToSoil { get; set; }
        public List<Map> SoilToFertilizer { get; set; }
        public List<Map> FertilizerToWater { get; set; }
        public List<Map> WaterToLight { get; set; }
        public List<Map> LightToTemp { get; set; }
        public List<Map> TempToHumidity { get; set; }
        public List<Map> HumidityToLocation { get; set; }

        private List<Map> GetTheMaps(List<string> input, string nameOfSection)
        {
            var maps = new List<Map>();

            bool isTargetSection = false;
            foreach (var line in input)
            {
                if (line == nameOfSection)
                {
                    isTargetSection = true;
                }
                else if (string.IsNullOrEmpty(line))
                {
                    isTargetSection = false;
                }

                if (isTargetSection && line != nameOfSection)
                {
                    maps.Add(new Map(
                        long.Parse(line.Split(' ')[1]),
                        long.Parse(line.Split(' ')[0]),
                        long.Parse(line.Split(' ')[2])
                        ));
                }
            }
            return maps;
        }
    }

    public class SeedSchema
    {
        public SeedSchema(long seed, Maps maps)
        {
            this.SeedNumber = seed;
            this.SoilNumber = GetTheCorrespondingValue(SeedNumber, maps.SeedsToSoil);
            this.FertilizerNumber = GetTheCorrespondingValue(SoilNumber, maps.SoilToFertilizer);
            this.WaterNumber = GetTheCorrespondingValue(FertilizerNumber, maps.FertilizerToWater);
            this.LightNumber = GetTheCorrespondingValue(WaterNumber, maps.WaterToLight);
            this.TempNumber = GetTheCorrespondingValue(LightNumber, maps.LightToTemp);
            this.HumidityNumber = GetTheCorrespondingValue(TempNumber, maps.TempToHumidity);
            this.LocationNumber = GetTheCorrespondingValue(HumidityNumber, maps.HumidityToLocation);
        }

        long SeedNumber;
        long SoilNumber;
        long FertilizerNumber;
        long WaterNumber;
        long LightNumber;
        long TempNumber;
        long HumidityNumber;
        public long LocationNumber { get; set; }

        public long GetTheCorrespondingValue(long baseValue, List<Map> maps)
        {
            foreach(var map in maps)
            {
                if (baseValue > map.SourceStart && baseValue < map.SourceStart + map.Range)
                {
                    return map.DestinationStart + baseValue - map.SourceStart;
                }
            }
            return baseValue;
        }
    }

    public class Map
    {
        public Map(long source, long destination, long range) {
            this.SourceStart = source; 
            this.DestinationStart = destination;
            this.Range = range;
        }
        public long SourceStart { get; set; }
        public long DestinationStart { get; set; }
        public long Range { get; set; }
    }
}
