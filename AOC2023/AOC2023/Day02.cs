﻿using AOCShared;

namespace AOC2023
{
    internal class Day02 : Aoc<List<string>, int, int>
    {
        private const byte MaxRedValue = 12;
        private const byte MaxGreenValue = 13;
        private const byte MaxBlueValue = 14;

        public Day02(string fileName) : base(fileName)
        {
        }

        public override int GetResultPart1()
        {
            int possibleGamesSum = 0;

            foreach (var singleGame in Input)
            {
                possibleGamesSum += GetValueOfPossibleGame(singleGame);
            }

            return possibleGamesSum;
        }

        private int GetValueOfPossibleGame(string singleGames)
        {
            int valueOfTheGame = Convert.ToInt16(singleGames.Split(':')[0].Split(' ')[1]);
            var cubeSets = singleGames.Split(":")[1].Split(';').ToList();

            return ValidSets(cubeSets) ? valueOfTheGame : 0;
        }

        private bool ValidSets(List<string> cubeSets)
        {
            foreach(var set in cubeSets) {
                var colors = set.Split(',').ToList();
                if (!colors.All(IsValidColor))
                {
                    return false;
                }
            }
            
            return true;
        }

        private bool IsValidColor(string color)
        {
            var quantity = Convert.ToInt16(color.Split(' ')[1].Trim());
            var colorName = color.Split(' ')[2];

            switch (colorName)
            {
                case "red":
                    return quantity <= MaxRedValue;
                case "green":
                    return quantity <= MaxGreenValue;
                case "blue":
                    return quantity <= MaxBlueValue;
                default:
                    return false;
            }
        }
        
        public override int GetResultPart2()
        {
            int sumOfPower = 0;

            foreach (var singleGame in Input)
            {
                sumOfPower += PowerOfFewestCubeNumber(singleGame);
            }

            return sumOfPower;
        }

        private int PowerOfFewestCubeNumber(string singleGames)
        {
            int redHigh = 0;
            int greenHigh = 0;
            int blueHigh = 0;

            List<string> gameSets = singleGames.Split(':')[1].Split(';').ToList();
            foreach (var set in gameSets)
            {
                UpdateHighestValues(set, ref redHigh, ref greenHigh, ref blueHigh);    
            }

            return redHigh * greenHigh * blueHigh;
        }

        private void UpdateHighestValues(string set, ref int redHigh, ref int greenHigh, ref int blueHigh)
        {
            var colors = set.Split(',').ToList();

            foreach (var color in colors)
            {
                var quantity = Convert.ToInt16(color.Split(' ')[1].Trim());
                var colorName = color.Split(' ')[2];
                switch (colorName)
                {
                    case "red":
                        if (quantity > redHigh)
                        {
                            redHigh = quantity;
                        };
                        break;
                    case "green":
                        if (quantity > greenHigh)
                        {
                            greenHigh = quantity;
                        };
                        break;
                    case "blue":
                        if (quantity > blueHigh)
                        {
                            blueHigh = quantity;
                        };
                        break;
                }
            }
        }
    }
}
