﻿namespace AOC2023.Models
{
    internal class PokerHand
    {
        private const char jokerSign = 'J';
        private bool _isJokerIncluded;
        private int _bid;
        private Dictionary<string, char> _cardsMapping = new Dictionary<string, char>()
        {
                { "01", '2' },
                { "02", '3' },
                { "03", '4' },
                { "04", '5' },
                { "05", '6' },
                { "06", '7' },
                { "07", '8' },
                { "08", '9' },
                { "09", 'T' },
                { "11", 'Q' },
                { "12", 'K' },
                { "13", 'A' }
        };

        public long HandValue { get; set; } 

        public PokerHand(string cards, int bid, bool isJokerIncluded)
        {
            _isJokerIncluded = isJokerIncluded;
            _bid = bid;
            CompleteTheMapping(isJokerIncluded);
            HandValue = GetTheValue(cards);
        }

        public int CalculateWinning(int rank)
        {
            return _bid * rank;
        }

        private void CompleteTheMapping(bool isJoker)
        {
            if (isJoker)
            {
                _cardsMapping.Add("00", jokerSign);
            }
            else
            {
                _cardsMapping.Add("10", jokerSign);
            }
        }

        private long GetTheValue(string cards)
        {
            int handValue = GetTheHandType(CardProxy(cards,_isJokerIncluded));
            string cardsValue = GetCardsValue(cards);
            return long.Parse($"{handValue}{cardsValue}");
        }

        private string CardProxy(string cards, bool isJoker)
        {
            return isJoker ? AdjustCards(cards) : cards;
        }

        private string AdjustCards(string cards)
        {
            int jokers = cards.Where(card => card == jokerSign).Count();

            switch(jokers)
            {
                case 0: return cards;
                case 5: return "AAAAA";
                default:
                    char desiredCard = GetTheDesiredCard(cards);
                    return cards.Replace(jokerSign, desiredCard);
            }
        }

        private char GetTheDesiredCard(string cards)
        {
            List<int> listOfCardsValue = GetListOfCardsValue(cards.Replace(jokerSign.ToString(), ""));

            var countsCardsOccurrences = listOfCardsValue.GroupBy(card => card)
                              .ToDictionary(group => group.Key, group => group.Count());

            List<int> sortedList = listOfCardsValue.OrderByDescending(x => countsCardsOccurrences[x])
                                        .ThenByDescending(x => x)
                                        .ToList();

            return GetCardForGivenValue(sortedList[0]);
        }

        private List<int> GetListOfCardsValue(string cards)
        {
            List<int> result = new List<int>();
            foreach (char card in cards)
            {
                result.Add(int.Parse(GetCardValue(card)));
            }
            return result;
        }

        private string GetCardsValue(string cards)
        {
            string value = string.Empty;
            foreach (char card in cards)
            {
                value += GetCardValue(card);
            }
            return value;
        }

        private string GetCardValue(char desiredCard)
        {
            return _cardsMapping.Where(card => card.Value == desiredCard).Select(card => card.Key).First();
        }

        private char GetCardForGivenValue(int cardValue)
        {
            string carDictValue = cardValue < 10 ? $"0{cardValue}" : cardValue.ToString();
            return _cardsMapping.Where(card => card.Key == carDictValue).Select(card => card.Value).First();
        }

        private int GetTheHandType(string cards)
        {
            if (IsFiveOfAKind(cards)) { return 6; }
            if (IsFourOfAKind(cards)) { return 5; }
            if (IsFullHouse(cards)) { return 4; }
            if (IsThreeOfKind(cards)) { return 3; }
            if (IsTwoPairs(cards)) { return 2; }
            if (IsOnePair(cards)) { return 1; }

            return 0;
        }

        private bool IsOnePair(string cards)
        {
            return cards.Distinct().Count() == 4; ;
        }

        private bool IsTwoPairs(string cards)
        {
            return cards.Distinct().Count() == 3;
        }

        private bool IsThreeOfKind(string cards)
        {
            return NumberOfTheSameCardAppearance(cards, 3) && !NumberOfTheSameCardAppearance(cards, 2);
        }

        private bool IsFullHouse(string cards)
        {
            return NumberOfTheSameCardAppearance(cards, 3) && NumberOfTheSameCardAppearance(cards, 2);
        }

        private bool IsFourOfAKind(string cards)
        {
            return NumberOfTheSameCardAppearance(cards, 4) && NumberOfTheSameCardAppearance(cards, 1);
        }

        private bool NumberOfTheSameCardAppearance(string cards, int appearance)
        {
            return cards.GroupBy(card => card).Any(group => group.Count() == appearance);
        }

        private bool IsFiveOfAKind(string cards)
        {
            return cards.Distinct().Count() == 1;
        }
    }
}
