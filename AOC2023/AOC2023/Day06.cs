﻿using AOCShared;

namespace AOC2023
{
    internal class Day06 : Aoc<List<string>, int, int>
    {
        public Day06(string fileName) : base(fileName)
        {
        }

        public override int GetResultPart1()
        {
            List<Race> races = GetRaces();
            int finalValue = 1;

            foreach (var race in races)
            {
                finalValue *= GetThePossibleRecordsOfRace(race);
            }

            return finalValue;
        }

        private List<Race> GetRaces()
        {
            var races = new List<Race>();
            int index = 0;
            var times = GetListOfIntFromTheString(Input[0]);
            var distances = GetListOfIntFromTheString(Input[1]);

            foreach (var time in times)
            {
                races.Add(new Race(time, distances[index]));
                index++;
            }

            return races;
        }

        private List<int> GetListOfIntFromTheString(string input)
        {
            return input.Split(" ").Where(x => x != "").Skip(1).Select(x => int.Parse(x)).ToList();
        }

        private int GetThePossibleRecordsOfRace(Race race)
        {
            List<long> distances = new List<long>();
            for (long i = 1; i < race.Time; i++)
            {
                distances.Add(CalculateTheDistance(i, race.Time));
            }

            return distances.Where(x => x > race.Distance).Count();
        }

        private long CalculateTheDistance(long pushTime, long raceTime)
        {
            return pushTime * (raceTime - pushTime);
        }

        public override int GetResultPart2()
        {
            Race race = GetTheRace();

            return GetThePossibleRecordsOfRace(race); ;
        }

        private Race GetTheRace()
        {            
            long raceTime = GetTheLongValueFromTheString(Input[0]);
            long raceDistance = GetTheLongValueFromTheString(Input[1]);

            return new Race(raceTime, raceDistance);
        }

        private long GetTheLongValueFromTheString(string input)
        {
            return long.Parse(input.Split(':')[1].Replace(" ", ""));
        }
    }

    public class Race
    {        
        public long Time { get; }  
        public long Distance { get; }
        public Race(long time, long distance) {
            this.Time = time;
            this.Distance = distance;
        }
    }
}
