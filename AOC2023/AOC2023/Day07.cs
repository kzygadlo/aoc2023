﻿using AOC2023.Models;
using AOCShared;

namespace AOC2023
{
    internal class Day07 : Aoc<List<string>, int, int>
    {
        public Day07(string fileName) : base(fileName)
        {
        }

        public override int GetResultPart1()
        {
            var pokerHands = GetPokerHands(Input, false);

            return GetTheTotalWinning(pokerHands);
        }

        private int GetTheTotalWinning(List<PokerHand> pokerHands)
        {
            int totalWinning = 0;
            int handIndex = 1;
            var sortedHands = pokerHands.OrderBy(x => x.HandValue).ToList();

            foreach (PokerHand hand in sortedHands)
            {
                totalWinning += hand.CalculateWinning(handIndex++);
            }

            return totalWinning;
        }

        private List<PokerHand> GetPokerHands(List<string> input, bool isJoker)
        {
            List<PokerHand> result = new List<PokerHand>();
            if (input != null)
            {
                foreach (var item in input)
                {
                    result.Add(new PokerHand(
                        item.Split(' ')[0],
                        int.Parse(item.Split(' ')[1]),
                        isJoker
                        ));
                }
            }

            return result;
        }

        public override int GetResultPart2()
        {
            var pokerHands = GetPokerHands(Input, true);

            return GetTheTotalWinning(pokerHands);
        }
    }
}
