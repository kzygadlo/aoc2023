﻿using AOCShared;

namespace AOC2023
{
    internal class Day04 : Aoc<List<string>, int, int>
    {
        private List<ScratchCard> _scratchCardsList = new List<ScratchCard>();
        private Queue<ScratchCard> _scratchCardsQueue = new Queue<ScratchCard>();
        private List<int> _winningCards = new List<int>();
        
        public Day04(string fileName) : base(fileName)
        {
            PopulateScratchCard();
        }

        private void PopulateScratchCard()
        {
            int index = 1;
            foreach(var item in Input)
            {
                var numbersArrays = item.Split(':')[1].Split('|');
                List<int> winningNumbers = numbersArrays[0].Split(' ').Where(x => x != "").ToList().ConvertAll(int.Parse);
                List<int> myNumbers = numbersArrays[1].Split(' ').Where(x => x != "").ToList().ConvertAll(int.Parse);

                _scratchCardsList.Add(new ScratchCard(index, winningNumbers, myNumbers));
                _scratchCardsQueue.Enqueue(new ScratchCard(index, winningNumbers, myNumbers));
                index++;
            }
        }

        public override int GetResultPart1()
        {
            return _scratchCardsList.Sum(x => x.GetTheValueOfThePrice());
        }

        public override int GetResultPart2()
        {
            while (_scratchCardsQueue.Count > 0)
            {
                var card = _scratchCardsQueue.Dequeue();
                int indexOfCard = card.Id;
                _winningCards.Add(card.Id);
                foreach(var item in card.Matches)
                {
                    indexOfCard++;
                    _scratchCardsQueue.Enqueue(_scratchCardsList.FirstOrDefault(x => x.Id == indexOfCard));
                }
            }
            return _winningCards.Count();
        }
    }

    public class ScratchCard
    {
        public int Id { get; }
        public List<int> Matches { get; }

        public ScratchCard(int id, List<int> winningNumbers, List<int> myNumbers)
        {
            this.Id = id;
            this.Matches = myNumbers.Intersect(winningNumbers).ToList();
        }
        public int GetTheValueOfThePrice()
        {
            return Matches.Any() ? Matches.Aggregate(1, (current, _) => current *2)/2 : 0;
        }
    }
}
