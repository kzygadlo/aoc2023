using System.Numerics;
using AOCShared;

namespace AOC2023
{
    internal class Day08 : Aoc<List<string>, int, long>
    {
        private const string StartingPoint = "AAA";
        private const string EndingPoint = "ZZZ";
        private const char Right = 'R';
        private readonly Queue<char> _movementSequence;
        private readonly List<Instruction> _instructions = new List<Instruction>();
        private delegate bool MoveCondition(string point);

        public Day08(string fileName) : base(fileName)
        {
            _movementSequence = GetSequence(Input);
            _instructions = GetInstructions(Input.SkipWhile(line => line.IndexOf('=') == -1).ToList());
        }

        private Queue<char> GetSequence(List<string> input)
        {
            return new Queue<char>(input[0]);
        }

        private List<Instruction> GetInstructions(List<string> list)
        {
            var instructions = new List<Instruction>();

            list.ForEach(item => {
                int startIndexForLeft = item.IndexOf('(');
                int endIndexForRignt = item.IndexOf(')');
                instructions.Add(new Instruction(
                    item.Split(' ')[0],
                    item.Substring(startIndexForLeft+1, 3),
                    item.Substring(endIndexForRignt-3, 3)
                ));
            });

            return instructions;
        }

        public override int GetResultPart1()
        {
            string currentPoint = _instructions.Where(i=> i.StartingPoint == StartingPoint).First().StartingPoint;
            if (!currentPoint.Any()) 
                { throw new NullReferenceException("Starting point AAA doesn't exist in the input."); }

            return GetMovesNumber(currentPoint, point => point!= EndingPoint);
        }

        private int GetMovesNumber(string currentPoint, MoveCondition condition)
        {
            int moveCounter = 0;
            do {
                moveCounter++;
                var direction = _movementSequence.Dequeue();
                Instruction instruction = _instructions.Where(i => i.StartingPoint == currentPoint).First();
                currentPoint = direction == Right ? instruction.RightDirection : instruction.LeftDirection;
                _movementSequence.Enqueue(direction);          
            } while (condition(currentPoint));

             return moveCounter;
        }

        public override long GetResultPart2()
        {
            List<string> nodes = _instructions
                .Where(i => i.StartingPoint[2] == Globals.Alphabet.First())
                .Select(i=> i.StartingPoint).ToList();
            
            if (!nodes.Any()) 
                { throw new NullReferenceException("Starting point thats ends with 'A' doesn't exist in the input."); }
            
            List<BigInteger> movesCount = new List<BigInteger>();
            nodes.ForEach(node => {
                movesCount.Add(GetMovesNumber(node, node => node[2] != Globals.Alphabet.Last()));
            });

            return GetLeastCommonMultiple(movesCount);
        }

        private long GetLeastCommonMultiple(List<BigInteger> movesCount)
        {
            int greatestCommonDivisor = GetGCD(movesCount);

            BigInteger result = movesCount
                .Select(m => m/greatestCommonDivisor)
                .Aggregate((current, next) => current * next) * greatestCommonDivisor;

            return (long) result;
        }

        private int GetGCD(List<BigInteger> movesCount)
        {
            BigInteger GCD = BigInteger.GreatestCommonDivisor(movesCount[0], movesCount[1]);

            for (int i = 1; i< movesCount.Count; i++) {
                GCD = BigInteger.GreatestCommonDivisor(GCD, movesCount[i]);
            }

            return (int)GCD;
        }
    }

    public class Instruction {
        public string StartingPoint { get; set; }
        public string LeftDirection { get; set; }
        public string RightDirection { get; set; } 

        public Instruction(string startingPoint, string leftDirection, string rightDirection) {
            StartingPoint = startingPoint;
            LeftDirection = leftDirection;
            RightDirection = rightDirection;
        }
    }
}
