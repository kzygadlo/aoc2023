﻿using AOCShared;

namespace AOC2023
{
    internal class Day03 : Aoc<List<string>, int, long>
    {
        private readonly int _xSize;
        private readonly int _ySize;
        private const char SpecialSign = '*';
        private const char DefaultSign = '.';

        public Day03(string fileName) : base(fileName)
        {
            _xSize = Input[0].Length;
            _ySize = Input.Count;
        }

        private int[,] GetPuzzleInputMatrix(List<string> input)
        {
            var result = new int[_ySize+2, _xSize+2].FillWithValue(int.MaxValue);
            var specialSignIndex = -1;
            for (var y = 1; y <= _ySize; y++)
            {
                for (var x = 1; x <= _xSize; x++)
                {
                    if (char.IsNumber(input[y-1][x-1]))
                    {
                        result[y, x] = int.Parse(input[y-1][x-1].ToString());
                    }
                    else if (IsInvalid(input[y-1][x-1]))
                    {
                        result[y, x] = specialSignIndex--;
                    }
                }
            }
            
            return result;
        }

        private bool IsInvalid(char sign)
        {
            return !char.IsNumber(sign) && sign != DefaultSign;
        }

        public override int GetResultPart1()
        {
            var puzzleInput = GetPuzzleInputMatrix(Input);
            List<Gear> result = GetValidNumbers(puzzleInput);
            
            return result.Select(x=>x.Value).Sum();
        }
        private List<Gear> GetValidNumbers(int[,] puzzleInput)
        {
            List<Gear> numbers = new List<Gear>();
            int isValid = 0;
            string number = "";

            for (var y = 0; y <= _ySize; y++)
            {
                for (var x = 0; x <= _xSize; x++)
                {
                    if (puzzleInput[y, x] >= 0 && puzzleInput[y,x] < 10)
                    {
                        number += puzzleInput[y, x];
                        if (isValid >-1)
                        {
                            isValid = CheckIfValid(y, x, puzzleInput);
                        }
                    }
                    else if (number != "")
                    {
                        if (isValid < 0)
                        {
                            numbers.Add(new Gear(Convert.ToInt32(number), isValid));
                        }
                        number = "";
                        isValid = 0;
                    };
                }
            }

            return numbers;
        }

        private int CheckIfValid(int y, int x, int[,] puzzleInput)
        {
            if (y > 0 && puzzleInput[y - 1, x] < 0) { 
                return puzzleInput[y - 1, x]; };
            if (x > 0 && puzzleInput[y, x - 1] < 0) { 
                return puzzleInput[y, x - 1]; };
            if (x < _xSize - 1 && puzzleInput[y, x + 1] < 0) { 
                return puzzleInput[y, x + 1]; };
            if (y < _ySize - 1 && puzzleInput[y + 1, x] < 0) { 
                return puzzleInput[y + 1, x]; };
            if (y > 0 && x > 0 && puzzleInput[y - 1, x - 1] < 0) { 
                return puzzleInput[y - 1, x - 1]; };
            if (y < _ySize - 1 && x > 0 && puzzleInput[y + 1, x - 1] < 0) { 
                return puzzleInput[y + 1, x - 1]; };
            if (y > 0 && x < _xSize - 1 && puzzleInput[y - 1, x + 1] < 0) { 
                return puzzleInput[y - 1, x + 1]; };
            if (y < _ySize - 1 && x < _xSize - 1 && puzzleInput[y + 1, x + 1] < 0) { 
                return puzzleInput[y + 1, x + 1]; };

            return 0;
        }

        public override long GetResultPart2()
        {
            var inputAdjustment = AdjustInput(Input);
            var puzzleInput = GetPuzzleInputMatrix(inputAdjustment);
            List<Gear> result = GetValidNumbers(puzzleInput);

            return GetGearValue(result);
        }

        private long GetGearValue(List<Gear> gears)
        {
            long gearsValue = 0;

            var validGearsGroup = gears
                .GroupBy(x => x.Index)
                .Where(group => group.Count() == 2);
            
            foreach(var group in validGearsGroup)
            {
                long gearValue = 1;
                foreach (var gear in group)
                {
                    gearValue *= gear.Value;
                }
                gearsValue += gearValue;

            };

            return gearsValue;
        }

        private List<string> AdjustInput(List<string> input)
        {
            var result =  new List<string>();
            foreach (var line in input) {
                string newLine = new string(
                    line.Select(item => char.IsDigit(item) || item == SpecialSign ? item : DefaultSign).ToArray()
                    );
                result.Add(newLine);
            }

            return result;
        }
    }

    public class Gear
    {
        public int Value { get; }
        public int Index { get; }

        public Gear(int value, int index) {
            Value = value;
            Index = index;
        }
    }
}
