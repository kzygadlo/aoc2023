using AOCShared;

namespace AOC2023 {
    internal class Day09 : Aoc<List<string>, int, int>
    {
        private List<History> _histories; 
        public Day09(string fileName) : base(fileName)
        {
            _histories = GetHistories(Input);
        }

        private List<History> GetHistories(List<string> input)
        {
            var histories = new List<History>();
            if(input == null) {
                throw new NullReferenceException("The input file is empty.");
            }

            foreach (var item in input) {
                List<int> sequence = item.Split(' ').Select(int.Parse).ToList();
                histories.Add(new History(sequence));
            }

            return histories;
        }

        public override int GetResultPart1()
        {
            int historiesValue = 0;
            foreach(var history in _histories) {
                historiesValue += history.GetNextHistoryValue();
            }
            return historiesValue;
        }

        public override int GetResultPart2()
        {
            int historiesValue = 0;
            foreach(var history in _histories) {
                historiesValue += history.GetPreviousHistoryValue();
            }
            return historiesValue;
        }
    }

    internal class History {
        private List<int> _initialSequence ;
        private List<List<int>> _sequences;

        public History(List<int> initialSequence) {
            _initialSequence = initialSequence;
            _sequences = GetSequences(_initialSequence);
        }

        private List<List<int>> GetSequences(List<int> initialSequence)
        {
            List<List<int>> sequences = new List<List<int>> {initialSequence};

            while (!IsFinalSequence(initialSequence)) {
                initialSequence = BuildNewSequence(initialSequence);
                sequences.Add(initialSequence);
            }
            return sequences;
        }

        private List<int> BuildNewSequence(List<int> initialSequence)
        {
            var newSequence = new List<int>();
            
            for (int i = 1; i<initialSequence.Count; i++) {
                newSequence.Add(initialSequence[i]-initialSequence[i-1]);
            }

            return newSequence;
        }

        private bool IsFinalSequence(List<int> initialSequence)
        {
            return initialSequence.All(number=> number==0);
        }

        public int GetNextHistoryValue() 
        {
            int value = 0;
            _sequences.ForEach(sequence => {
                value += sequence.Last();
            });
            return value;
        }

        internal int GetPreviousHistoryValue()
        {
            int value = 0;            
            for (int index = _sequences.Count() - 2; index >= 0; index--) {

                value = _sequences[index].First() - value;
            }
            return value;
        }
    }
}