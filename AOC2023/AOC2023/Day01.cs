﻿using AOCShared;

namespace AOC2023
{
    internal class Day01 : Aoc<List<string>, int, int>
    {
        private readonly Dictionary<int, string> _digitsDict = new Dictionary<int, string>
        {
            { 1, "one" },
            { 2, "two" },
            { 3, "three" },
            { 4, "four" },
            { 5, "five" },
            { 6, "six" },
            { 7, "seven" },
            { 8, "eight" },
            { 9, "nine" },
        };

        public Day01(string fileName) : base(fileName)
        {
        }

        public override int GetResultPart1()
        {
            int calibrationVal = 0;

            foreach(string line in Input)
            {
                calibrationVal += CalculateCalibrationValueForDigits(line);
            }
            return calibrationVal;
        }

        private int CalculateCalibrationValueForDigits(string line)
        {
            char leftDigit = FindLeftDigit(line);
            char rightDigit = FindRightDigit(line);
            
            return Convert.ToInt32($"{leftDigit}{rightDigit}");
        }

        private char FindLeftDigit(string line)
        {
            return line.FirstOrDefault(c => Char.IsDigit(c));
        }

        private char FindRightDigit(string line)
        {
            return line.Last(c => Char.IsDigit(c));
        }

        public override int GetResultPart2()
        {
            int calibrationVal = 0;

            foreach (string line in Input)
            {
                calibrationVal += CalculateCalibrationValueForLetters(line);
            }

            return calibrationVal;
        }

        private int CalculateCalibrationValueForLetters(string line)
        {
            Dictionary<int, int> digitsDictionary = MapDigitsToIndexes(line);
            int leftDigit = digitsDictionary.OrderBy(item => item.Key).FirstOrDefault().Value;
            int rightDigit = digitsDictionary.OrderByDescending(item => item.Key).FirstOrDefault().Value;

            return Convert.ToInt32($"{leftDigit}{rightDigit}");
        }

        private Dictionary<int,int> MapDigitsToIndexes(string line)
        {
            Dictionary<int, int> foundDigits = new Dictionary<int, int>();
            foreach (var digit in _digitsDict)
            {
                SearchAndAddIndexForLetter(line, digit, foundDigits);
                SearchAndAddIndexForDigit(line, digit, foundDigits);
            }

            return foundDigits;
        }

        private void SearchAndAddIndexForDigit(string line, KeyValuePair<int, string> digit, Dictionary<int, int> foundDigits)
        {
            int indexOfDigit = line.IndexOf(digit.Key.ToString());
            if (indexOfDigit > -1)
            {
                foundDigits.Add(indexOfDigit, digit.Key);
            }
            while (indexOfDigit != -1)
            {
                indexOfDigit = line.IndexOf(digit.Key.ToString(), indexOfDigit + 1);
                if (indexOfDigit > -1)
                {
                    foundDigits.Add(indexOfDigit, digit.Key);
                }
            }
        }

        private void SearchAndAddIndexForLetter(string line, KeyValuePair<int, string> digit, Dictionary<int, int> foundDigits)
        {
            int indexOfLetter = line.IndexOf(digit.Value);
            if (indexOfLetter > -1)
            {
                foundDigits.Add(indexOfLetter, digit.Key);
            }
            while (indexOfLetter != -1)
            {
                indexOfLetter = line.IndexOf(digit.Value, indexOfLetter + digit.Value.Length);
                if (indexOfLetter > -1)
                {
                    foundDigits.Add(indexOfLetter, digit.Key);
                }
            }
        }
    }
}
