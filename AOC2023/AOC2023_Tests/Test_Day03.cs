using AOC2023;

namespace AOC2023_Tests
{
    public class Test_Day03
    {
        private const string FileName = "Day03-sample";

        [Test]
        public void Part1()
        {
            var day = new Day03(FileName);

            Assert.That(day.GetResultPart1(), Is.EqualTo(4361));
        }

        [Test]
        public void Part2()
        {
            var day = new Day03(FileName);

            Assert.That(day.GetResultPart2(), Is.EqualTo(467835));
        }
    }
}