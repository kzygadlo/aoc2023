using AOC2023;

namespace AOC2023_Tests
{
    public class Test_Day04
    {
        private const string FileName = "Day04-sample";

        [Test]
        public void Part1()
        {
            var day = new Day04(FileName);

            Assert.That(day.GetResultPart1(), Is.EqualTo(13));
        }

        [Test]
        public void Part2()
        {
            var day = new Day04(FileName);

            Assert.That(day.GetResultPart2(), Is.EqualTo(30));
        }
    }
}