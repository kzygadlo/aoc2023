using AOC2023;

namespace AOC2023_Tests
{
    public class Test_Day09
    {
        private const string FileName = "Day09-sample";

        [Test]
        public void Part1()
        {
            var day = new Day09(FileName);

            Assert.That(day.GetResultPart1(), Is.EqualTo(114));
        }

        [Test]
        public void Part2()
        {
            var day = new Day09(FileName);

            Assert.That(day.GetResultPart2(), Is.EqualTo(2));
        }
    }
}