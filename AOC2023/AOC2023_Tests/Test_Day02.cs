using AOC2023;

namespace AOC2023_Tests
{
    public class Test_Day02
    {
        private const string FileName = "Day02-sample";

        [Test]
        public void Part1()
        {
            var day = new Day02(FileName);

            Assert.That(day.GetResultPart1(), Is.EqualTo(8));
        }

        [Test]
        public void Part2()
        {
            var day = new Day02(FileName);

            Assert.That(day.GetResultPart2(), Is.EqualTo(2286));
        }
    }
}