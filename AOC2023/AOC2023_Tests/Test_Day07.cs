using AOC2023;

namespace AOC2023_Tests
{
    public class Test_Day07
    {
        private const string FileName = "Day07-sample";

        [Test]
        public void Part1()
        {
            var day = new Day07(FileName);

            Assert.That(day.GetResultPart1(), Is.EqualTo(6440));
        }

        [Test]
        public void Part2()
        {
            var day = new Day07(FileName);

            Assert.That(day.GetResultPart2(), Is.EqualTo(5905));
        }
    }
}