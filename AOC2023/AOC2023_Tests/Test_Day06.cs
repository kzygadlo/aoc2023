using AOC2023;

namespace AOC2023_Tests
{
    public class Test_Day06
    {
        private const string FileName = "Day06-sample";

        [Test]
        public void Part1()
        {
            var day = new Day06(FileName);

            Assert.That(day.GetResultPart1(), Is.EqualTo(288));
        }

        [Test]
        public void Part2()
        {
            var day = new Day06(FileName);

            Assert.That(day.GetResultPart2(), Is.EqualTo(71503));
        }
    }
}