using AOC2023;

namespace AOC2023_Tests
{
    public class Test_Day01
    {
        private const string FileName = "Day01-sample";
        private const string FileNamePart2 = "Day01-sample_part2";

        [Test]
        public void Part1()
        {
            var day = new Day01(FileName);

            Assert.That(day.GetResultPart1(), Is.EqualTo(142));
        }

        [Test]
        public void Part2()
        {
            var day = new Day01(FileNamePart2);

            Assert.That(day.GetResultPart2(), Is.EqualTo(281));
        }
    }
}