using AOC2023;

namespace AOC2023_Tests
{
    public class Test_Day05
    {
        private const string FileName = "Day05-sample";

        [Test]
        public void Part1()
        {
            var day = new Day05(FileName);

            Assert.That(day.GetResultPart1(), Is.EqualTo(35));
        }

        //[Test]
        //public void Part2()
        //{
        //    var day = new Day05(FileName);

        //    Assert.That(day.GetResultPart2(), Is.EqualTo(30));
        //}
    }
}