using AOC2023;

namespace AOC2023_Tests
{
    public class Test_Day08
    {
        private const string FileName = "Day08-sample";
        private const string FileNamePart2 = "Day08-sample_part2";

        [Test]
        public void Part1()
        {
            var day = new Day08(FileName);

            Assert.That(day.GetResultPart1(), Is.EqualTo(6));
        }

        [Test]
        public void Part2()
        {
            var day = new Day08(FileNamePart2);

            Assert.That(day.GetResultPart2(), Is.EqualTo(6));
        }
    }
}